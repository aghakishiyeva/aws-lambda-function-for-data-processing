use lambda_http::{service_fn, run, IntoResponse, Request, Response, Body};
use serde_json::{Value, json};
use lambda_runtime::Error;


async fn function_handler(event: Request) -> Result<impl IntoResponse, Error> {
    // Treat the body as binary data and attempt to convert it to a string
    let body_str = match event.body() {
        Body::Text(text) => text,
        Body::Binary(binary) => std::str::from_utf8(binary).unwrap_or("{}"),
        _ => "{}",
    };

    let json_body: Value = serde_json::from_str(body_str).unwrap_or(json!({ "name": "world" }));

    // Extract the name from the JSON body
    let name = json_body["name"].as_str().unwrap_or("world");

    // Construct a personalized message
    let message = format!("Hello {}, this is an AWS Lambda HTTP request", name);

    // Construct a JSON response
    let response_body = json!({ "message": message }).to_string();

    Ok(Response::builder()
        .status(200)
        .header("Content-Type", "application/json")
        .body(Body::from(response_body))
        .expect("Failed to render response"))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await?;
    Ok(())
}
