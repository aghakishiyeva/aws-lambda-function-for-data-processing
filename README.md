## AWS Lambda Function for Data Processing

This project demonstrates a simple AWS Lambda function written in Rust using the `lambda_http` and `lambda_runtime` crates to process and transform data.

### Dependencies:
- `lambda_http` (0.9.2): A library for writing serverless applications with AWS Lambda and API Gateway integration.
- `lambda_runtime` (0.9.1): Provides the runtime for AWS Lambda functions written in Rust.
- `tokio` (version 1): An asynchronous runtime for Rust.
- `tracing` (0.1): A framework for instrumenting Rust programs to collect structured, event-based diagnostic information.
- `tracing-subscriber` (0.3): A subscriber for the `tracing` framework to configure how trace data is collected.
- `serde` (1.0): A framework for serializing and deserializing Rust data structures.
- `serde_json` (1.0): A JSON serialization and deserialization library for Rust.

### Usage:
1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Make sure you have Rust and Cargo installed on your system.
4. Run `cargo build --release` to build the project.
5. Run `cargo lambda build --release` to compile the project for deployment on AWS Lambda.
6. Test the Lambda function locally using `cargo lambda start`.
7. Deploy the Lambda function to AWS using the AWS Management Console or the AWS CLI.
8. Integrate the Lambda function with API Gateway for HTTP access.

### Example Request:

```bash
curl -X POST \
  http://localhost:9000 \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "Gunel"
  }'
 ``` 


### Example Response:

```
{
"message": "Hello Gunel, this is an AWS Lambda HTTP request"
}
```